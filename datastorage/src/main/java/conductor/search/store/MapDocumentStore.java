package conductor.search.store;

import conductor.search.exception.DuplicateDocumentKeyException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapDocumentStore implements DocumentStore {
    private final Map<String, String> documentsMap = new HashMap<>(1000);

    @Override
    public void saveDocument(String key, String document) throws DuplicateDocumentKeyException {
        String result = documentsMap.putIfAbsent(key, document);
        if (result != null) {
            throw new DuplicateDocumentKeyException();
        }
    }

    @Override
    public String getDocument(String key) {
        return documentsMap.get(key);
    }

    @Override
    public List<String> getAllDocumentKeys() {
        return new ArrayList<>(documentsMap.keySet());
    }
}
