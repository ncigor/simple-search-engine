## Simple Search Engine
This is a very simple versin of search engine

## Installation
To compile source code into a jar file, please run script  
    `sh gradlew build` on Mac OS  
    `gradlew build` on Windows OS  
This instruction collect all required libs and package all into a file by location `build/libs/server.jar`
    
## Run App
If you have compiled file `build/libs/server.jar` you can run it with command  
    `java -jar build/libs/server.jar`   
The app starts on 8080 port. (http://localhost:8080/)  


## Compile and run App
Also you can compile and run app with a single command
`sh gradlew bootRun`

Use `-x test` parameters to skip test
`sh gradlew bootRun -x test`

Also the project requires _Java11_ and maybe it is not defined by default. 
In this case you can use parameter `-Dorg.gradle.java.home`.  
F.e.: `sh gradlew bootRun -x test -Dorg.gradle.java.home=/Library/Java/JavaVirtualMachines/jdk-12.jdk/Contents/Home`


## Additional Links
[CLI client](
https://gitlab.com/ncigor/simple-search-engine-client)
