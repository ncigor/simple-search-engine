package conductor.search.store;

import conductor.search.exception.DuplicateDocumentKeyException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MapDocumentStoreTest {
    private MapDocumentStore mapDocumentStore;

    @BeforeEach
    void init() {
        mapDocumentStore = new MapDocumentStore();
    }

    @Test
    @DisplayName("save documents and read saved documents")
    void testSaveDocument() {
        String document1Text = "some doc value 1828, 2882   9  3939";
        mapDocumentStore.saveDocument("key1", document1Text);
        String documentByKey1 = mapDocumentStore.getDocument("key1");
        Assertions.assertEquals(document1Text, documentByKey1);
    }

    @Test
    @DisplayName("Should generate exception on save document with non-unique key")
    void testSaveDuplicateDocument() {
        String key = "key1";
        mapDocumentStore.saveDocument(key, "text");
        assertThrows(DuplicateDocumentKeyException.class,
                () -> mapDocumentStore.saveDocument(key, "document2"));
    }

    @Test
    void getAllDocumentKeys() {
        mapDocumentStore.saveDocument("key1", "txt1");
        mapDocumentStore.saveDocument("key2", "txt2");
        mapDocumentStore.saveDocument("key3", "txt1");
        mapDocumentStore.saveDocument("key4", "txt3");

        List<String> allDocumentKeys = mapDocumentStore.getAllDocumentKeys();
        assertEquals(4, allDocumentKeys.size());
        assertTrue(allDocumentKeys.containsAll(Arrays.asList("key1", "key2", "key3", "key4")));
    }
}