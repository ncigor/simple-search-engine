package conductor.search.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import conductor.search.bean.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration
public class ApplicationTest {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Test
    void addDocument() throws Exception {
        indexDocument("key1", "some text").andExpect(status().isOk());
        indexDocument("key2", "unique text very long text").andExpect(status().isOk());

        getDocumentByKey("key1")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("some text"));

        getDocumentByKey("key2")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("unique text very long text"));

        findDocumentKeys("text").andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]").value(contains("key1", "key2")));

        findDocumentKeys("some text").andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]").value(contains("key1")))
                .andExpect(jsonPath("$.[*]").value(not(contains("key2"))));
    }

    private ResultActions indexDocument(String key1, String text) throws Exception {
        Document document = new Document();
        document.setText(text);
        return mvc.perform(MockMvcRequestBuilders.post("/documents/" +
                key1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(document))
        );
    }

    private ResultActions getDocumentByKey(String key) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get("/documents/" + key)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            );
    }

    private ResultActions findDocumentKeys(String query) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get("/documents")
                .param("query", query)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
        );
    }
}