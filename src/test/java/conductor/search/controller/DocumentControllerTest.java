package conductor.search.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import conductor.search.bean.Document;
import conductor.search.service.DocumentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration
public class DocumentControllerTest {
    private ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private DocumentService documentService;

    @Autowired
    private MockMvc mvc;

    @Test
    void addDocument() throws Exception {
        Document document = new Document();
        document.setText("some text");
        mvc.perform(MockMvcRequestBuilders.post("/documents/key1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(document))
        ).andExpect(status().isOk());
    }

    @Test
    void getDocument() throws Exception {
        Mockito.when(documentService.getDocument(any()))
            .thenReturn("document text");

        mvc.perform(MockMvcRequestBuilders.get("/documents/key1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk())
        .andExpect(jsonPath("$.text").value("document text"));
    }

    @Test
    void findDocuments() throws Exception {
        Mockito.when(documentService.findDocuments(any()))
            .thenReturn(Arrays.asList("k1", "k2", "k3"));

        mvc.perform(MockMvcRequestBuilders.get("/documents")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk())
        .andExpect(jsonPath("$.[0]").value("k1"))
        .andExpect(jsonPath("$.[1]").value("k2"))
        .andExpect(jsonPath("$.[2]").value("k3"));
    }
}