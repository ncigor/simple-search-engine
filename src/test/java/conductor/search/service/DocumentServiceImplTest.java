package conductor.search.service;

import conductor.search.index.DocumentIndexer;
import conductor.search.store.DocumentStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DocumentServiceImplTest {
    @Mock
    DocumentStore documentStore;

    @Mock
    DocumentIndexer documentIndexer;

    @InjectMocks
    DocumentServiceImpl documentService;


    @Test
    void testAddDocument() {
        String document_key = "document key";
        String document_text = "document text";

        documentService.addDocument(document_key, document_text);

        verify(documentStore, times(1)).saveDocument(document_key, document_text);
        verify(documentIndexer, times(1)).index(document_key, document_text);
    }

    @Test
    void testGetDocument() {
        String document_key = "document key";
        String document_text = "document text";

        when(documentStore.getDocument(document_key)).thenReturn(document_text);

        String result = documentService.getDocument(document_key);
        Assertions.assertEquals(document_text, result);
        verify(documentStore, times(1)).getDocument(document_key);
    }

    @Test
    void testFindDocuments() {
        List<String> expectedResults = Arrays.asList("k1", "k2", "k3", "k4");

        String query = "query text";
        when(documentIndexer.find(query)).thenReturn(expectedResults);

        List<String> result = documentService.findDocuments(query);
        Assertions.assertIterableEquals(expectedResults, result);
        verify(documentIndexer, times(1)).find(query);
        verify(documentStore, never()).getAllDocumentKeys();
    }

    @Test
    @DisplayName("Test find docs with null query string")
    void testFindDocuments_nullQuery() {
        List<String> expectedResults = Arrays.asList("k1", "k2", "k3", "k4");

        when(documentStore.getAllDocumentKeys()).thenReturn(expectedResults);

        List<String> result = documentService.findDocuments(null);
        Assertions.assertIterableEquals(expectedResults, result);
        verify(documentIndexer, never()).find(any());
        verify(documentStore, times(1)).getAllDocumentKeys();
    }
}