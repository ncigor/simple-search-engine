package conductor.search.bean;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Document {
    @NotNull
    @NotBlank
    private String text;

    public Document(String text) {
        this.text = text;
    }

    public Document() {
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
