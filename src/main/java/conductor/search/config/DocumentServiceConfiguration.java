package conductor.search.config;

import conductor.search.index.DocumentIndexer;
import conductor.search.index.MapDocumentIndexer;
import conductor.search.index.ThreadDocumentIndexer;
import conductor.search.store.MapDocumentStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DocumentServiceConfiguration {
    @Bean
    DocumentIndexer documentIndexer() {
        return new ThreadDocumentIndexer(
                mapDocumentIndexer()
        );
    }

    @Bean
    MapDocumentIndexer mapDocumentIndexer() {
        return new MapDocumentIndexer();
    }

    @Bean
    MapDocumentStore mapDocumentStore() {
        return new MapDocumentStore();
    }
}
