package conductor.search.service;

import java.util.List;

public interface DocumentService {
    void addDocument(String key, String document);

    String getDocument(String key);

    List<String> findDocuments(String query);
}
