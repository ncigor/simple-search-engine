package conductor.search.service;

import conductor.search.index.DocumentIndexer;
import conductor.search.store.DocumentStore;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.*;

@Service
public class DocumentServiceImpl implements DocumentService {

    private final DocumentStore documentStore;
    private final DocumentIndexer documentIndexer;

    public DocumentServiceImpl(DocumentStore documentStore,
                               DocumentIndexer documentIndexer) {
        this.documentStore = documentStore;
        this.documentIndexer = documentIndexer;
    }

    @Override
    public void addDocument(@NotNull @NotBlank String key, String document) {
        documentStore.saveDocument(key, document);
        documentIndexer.index(key, document);
    }

    @Override
    public String getDocument(String key) {
        return documentStore.getDocument(key);
    }

    @Override
    public List<String> findDocuments(String query) {
        if (query == null || query.isBlank()) {
            return documentStore.getAllDocumentKeys();
        }
        return documentIndexer.find(query);
    }
}
