package conductor.search.controller.error;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class ErrorModel implements Serializable {
	private static final long serialVersionUID = -8050273695577153661L;

	private final String message;

	public ErrorModel(String message) {
		this.message = message;
	}
}