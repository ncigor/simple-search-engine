package conductor.search.controller;

import conductor.search.controller.error.ErrorModel;
import conductor.search.exception.DuplicateDocumentKeyException;
import conductor.search.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ValidationException;

@org.springframework.web.bind.annotation.ControllerAdvice
@Slf4j
public class ControllerAdvice {
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ErrorModel processMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
        return new ErrorModel(exception.getMessage());
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorModel validationException(IllegalStateException ex) {
        log.debug("Unprocessable Entity: {}", ex.getMessage());
        return new ErrorModel(ex.getMessage());
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorModel processInternalServerError(Throwable ex) {
        log.error("Error", ex);
        return new ErrorModel(ex.getMessage());
    }

    @ExceptionHandler({MissingServletRequestParameterException.class, ValidationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorModel processMissingServletRequestParameterException(Throwable ex) {
        log.error("Error: MissingServletRequestParameterException: {}", ex.getMessage());
        return new ErrorModel(ex.getMessage());
    }

    @ExceptionHandler({DuplicateDocumentKeyException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ErrorModel processDuplicateDocumentKeyException(Throwable ex) {
        log.error("Error: DuplicateDocumentKeyException: {}", ex.getMessage());
        return new ErrorModel(ex.getMessage());
    }

    @ExceptionHandler({ResourceNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorModel processResourceNotFoundException(Throwable ex) {
        log.error("Error: ResourceNotFoundException: {}", ex.getMessage());
        return new ErrorModel(ex.getMessage());
    }
}
