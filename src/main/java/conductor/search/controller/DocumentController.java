package conductor.search.controller;

import conductor.search.bean.Document;
import conductor.search.exception.ResourceNotFoundException;
import conductor.search.service.DocumentService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/documents")
public class DocumentController {
    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @PostMapping("/{key}")
    public void addDocument(@PathVariable String key,
                            @Valid @RequestBody Document document) {
        documentService.addDocument(key, document.getText());
    }

    @GetMapping("/{key}")
    public Document getDocument(@PathVariable String key) {
        String document = documentService.getDocument(key);
        if (null == document) {
            throw new ResourceNotFoundException();
        }
        return new Document(document);
    }

    @GetMapping
    public List<String> findDocuments(@RequestParam(required = false) String query) {
        return documentService.findDocuments(query);
    }
}