package conductor.search.index;

import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;

class TokenIterator implements Iterator<String> {
    private static final Pattern TOKEN_SEPARATOR = Pattern.compile("[\\s.,]+");
    private final Scanner scanner;

    public TokenIterator(String document) {
        this.scanner = initScanner(document);
    }

    private Scanner initScanner(String document) {
        if (null == document) {
            document = "";
        }
        Scanner scanner = new Scanner(document);
        scanner.useDelimiter(TOKEN_SEPARATOR);
        return scanner;
    }

    @Override
    public boolean hasNext() {
        return scanner.hasNext();
    }

    @Override
    public String next() {
        return scanner.next();
    }
}
