package conductor.search.index;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.concurrent.ConcurrentHashMap.newKeySet;

public class MapDocumentIndexer implements DocumentIndexer {


    private final Map<String, Set<String>> indexMap = new ConcurrentHashMap<>(10000);

    @Override
    public void index(String key, String document) {
        TokenIterator tokenIterator = new TokenIterator(document);
        while (tokenIterator.hasNext()) {
            String token = prepareToken(tokenIterator.next());
            Set<String> keys = indexMap.computeIfAbsent(token, k -> newKeySet());
            keys.add(key);
        }
    }

    @Override
    public List<String> find(String query) {
        Objects.requireNonNull(query, "Query string is null");
        TokenIterator tokenIterator = new TokenIterator(query);
        List<String> results = null;
        while (tokenIterator.hasNext()) {
            List<String> keys = findByToken(tokenIterator.next());
            results = filterResults(results, keys);
            if (results.isEmpty()) {
                 break;
            }
        }
        if (null == results || results.isEmpty()) {
            return Collections.emptyList();
        }
        return results;
    }

    private String prepareToken(String token) {
        return token.toLowerCase();
    }

    private List<String> filterResults(List<String> results, List<String> keys) {
        if (results == null) {
            results = new ArrayList(keys);
        } else {
            results.retainAll(keys);
        }
        return results;
    }

    private List<String> findByToken(String query) {
        List<String> strings = Collections.emptyList();
        return Optional.of(query)
                .map(this::prepareToken)
                .filter(s -> !s.isBlank())
                .map(indexMap::get)
                .map(ArrayList::new)
                .map(list -> (List<String>) list)
                .orElse(strings);
    }
}
