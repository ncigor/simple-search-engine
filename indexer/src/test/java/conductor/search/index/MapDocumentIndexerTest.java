package conductor.search.index;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MapDocumentIndexerTest {

    private MapDocumentIndexer mapDocumentIndexer;

    @BeforeEach
    void init() {
        this.mapDocumentIndexer = new MapDocumentIndexer();
        Map<String, String> documentsMap = Map.of(
                "k1", "Str1 Str2 Str3, Str4",
                "k2", "Str1 Str2 Str3, Str4, Str5",
                "k3", "Str4, Str5, Str6"
        );
        documentsMap.forEach((key, document) -> {
            mapDocumentIndexer.index(key, document);
        });
    }

    @Test
    @DisplayName("Test search by simple query")
    void testFindData() {
        List<String> results = mapDocumentIndexer.find("Str2");
        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains("k1"));
        Assertions.assertTrue(results.contains("k2"));
        Assertions.assertFalse(results.contains("k3"));

        List<String> results2 = mapDocumentIndexer.find("Str6");
        Assertions.assertEquals(1, results2.size());
        Assertions.assertTrue(results2.contains("k3"));
    }

    @Test
    @DisplayName("Test search by simple query")
    void testFindByMultipleWords() {
        List<String> results = mapDocumentIndexer.find("Str1 Str2");
        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains("k1"));
        Assertions.assertTrue(results.contains("k2"));
        Assertions.assertFalse(results.contains("k3"));


        List<String> results2 = mapDocumentIndexer.find("Str1 Str5");
        Assertions.assertEquals(1, results2.size());
        Assertions.assertTrue(results2.contains("k2"));
    }

    @Test
    @DisplayName("Test search by simple query - diff order")
    void testFindByMultipleWordsDiffOrder() {
        List<String> results = mapDocumentIndexer.find("Str2 Str1");
        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains("k1"));
        Assertions.assertTrue(results.contains("k2"));
        Assertions.assertFalse(results.contains("k3"));
    }

    @Test
    @DisplayName("Test search ignore case")
    void testSearchIgnoreCase() {
        List<String> results = mapDocumentIndexer.find("sTr2 StR1");
        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains("k1"));
        Assertions.assertTrue(results.contains("k2"));
        Assertions.assertFalse(results.contains("k3"));
    }

    @Test
    @DisplayName("should return empty results")
    void findWithoutResult() {
        assertTrue(mapDocumentIndexer.find("Not Existing String").isEmpty());
        assertTrue(mapDocumentIndexer.find("Str6 Str1").isEmpty());
    }
}