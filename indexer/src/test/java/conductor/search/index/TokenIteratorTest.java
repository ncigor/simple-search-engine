package conductor.search.index;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class TokenIteratorTest {

    @Test
    void testIterator() {
        TokenIterator tokenIterator = new TokenIterator("Some document with different    spaces  \t  and  other signs... , , like comma , or digits 12882");
        List<String> expectedResults = Arrays.asList("Some", "document", "with", "different", "spaces", "and", "other", "signs", "like", "comma", "or", "digits", "12882");

        int i = 0;
        while (tokenIterator.hasNext()) {
            Assertions.assertEquals(expectedResults.get(i), tokenIterator.next());
            i++;
        }

        Assertions.assertEquals(expectedResults.size(), i);
    }

    @Test
    @DisplayName("Should work with null or empty documents")
    void testEmptyString() {
        Assertions.assertFalse(new TokenIterator(null).hasNext());
        Assertions.assertFalse(new TokenIterator("").hasNext());
    }
}