package conductor.search.store;

import java.util.List;

public interface DocumentStore {
    void saveDocument(String key, String document);

    String getDocument(String key);

    List<String> getAllDocumentKeys();
}
