package conductor.search.index;

import java.util.List;

public interface DocumentIndexer {
    void index(String key, String document);

    List<String> find(String query);
}
