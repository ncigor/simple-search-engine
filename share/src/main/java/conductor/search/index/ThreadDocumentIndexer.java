package conductor.search.index;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadDocumentIndexer implements DocumentIndexer {
    private static final int NUMBER_OF_THREADS_TO_INDEX = 4;

    private final ExecutorService executorService;
    private final DocumentIndexer documentIndexer;

    public ThreadDocumentIndexer(DocumentIndexer documentIndexer) {
        this.documentIndexer = Objects.requireNonNull(documentIndexer);
        this.executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS_TO_INDEX);
    }

    @Override
    public void index(String key, String document) {
        executorService.submit(() -> {
            documentIndexer.index(key, document);
        });
    }

    @Override
    public List<String> find(String query) {
        return documentIndexer.find(query);
    }
}
