package conductor.search.exception;

public class DuplicateDocumentKeyException extends RuntimeException {
    public DuplicateDocumentKeyException() {
        super();
    }
}
